#!/usr/bin/bash
export XDG_RUNTIME_DIR=/run/user/$(id -u)
export DOCKER_HOST="unix://$XDG_RUNTIME_DIR/podman/podman.sock"
cd "$(dirname "$0")"
docker-compose run server tar -czvf "/data/forgejo.tar.gz" "/data/git" "/data/gitea" "/data/ssh"
rclone copyto ./data/forgejo.tar.gz cflsousa:backups/forgejo.tar.gz -P
docker-compose run server rm /data/forgejo.tar.gz
