#!/usr/bin/bash
export XDG_RUNTIME_DIR=/run/user/$(id -u)
export DOCKER_HOST="unix://$XDG_RUNTIME_DIR/podman/podman.sock"
cd "$(dirname "$0")"
rclone copyto cflsousa:backups/forgejo.tar.gz ./data/forgejo.tar.gz -P
docker-compose run server rm -rf "/data/git" "/data/gitea" "/data/ssh"
docker-compose run server tar -xzvf /data/forgejo.tar.gz -C /
docker-compose run server rm /data/forgejo.tar.gz
