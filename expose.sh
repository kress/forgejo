#!/usr/bin/bash
state="pending"

cloudflared tunnel --url http://localhost:3000 2>&1 | while read -r line; do
  if [ "$state" = "pending" ]; then
    url=$(echo "$line" | grep -oh "https://\(.*\)trycloudflare.com")
    if [ -n "$url" ]; then
      telegram-send "Forgejo: $url"
      state="done"
    fi
  fi
done
